import getopt, sys
import json

contacts = []

def valid_number(phoneNumber):

    if not phoneNumber.isdigit():
        print("The phone number contains non-digit characters")
        return False
    
    return True


def create_contact(phoneNumber):
    # Check phoneNumber is a number
    if valid_number(phoneNumber):
        new = {}
        # Request other fields
        new["name"] = input("Name:")
        new["email"] = input("Email:")
        new["address"] = input("Address:")
        new["note"] = input("Note:")
        # Save and return
        contacts.append(new)
        print("New contact created successfully!")
    

def list_contacts():
    print("%s contacts in addressbook" % len(contacts))
    print(contacts)

def delete_contact(phoneNumber):
    if valid_number(phoneNumber):
        found = search_contact("phoneNumber", phoneNumber)
        if len(found) > 0: 
            print("%s contacts found. Are you sure you want to delete them all?" % len(found))
            decision = ""
            while decision not in ("y", "n"):
                decision = input("Y/N:").lower()
            
            if decision == "y":
                print("%s contacts removed" % len([contacts.remove(c) for c in found]))

            list_contacts()
        else:
            print("No contacts found. Try again")


def update_contact(phoneNumber):
    if valid_number(phoneNumber):
        found = search_contact("phoneNumber", phoneNumber)
        if len(found) > 0:
            print(found)
            newName = input("Enter new name:")
            newPhone = input("Enter new phone:")
            newEmail = input("Enter new email:")
            newAddress = input("Enter new address:")
            for c in found:
                c["name"] = newName
                c["phoneNumber"] = newPhone
                c["email"] = newEmail
                c["address"] = newAddress
        print("%s contacts updated" % len(found))
        list_contacts()


def search_contact(key, value):
    return [c for c in contacts if str(c[key]).lower().find(str(value).lower()) != -1]

def usage():
    inst = """
    -h or --help
    -l or --list
    -n or --new [phone number]
    -s or --search [name]
    -d or --delete [phone number]
    -u or --update [key]
    Examples:
    \tcontacts.py -n 985000454
    \tcontacts.py -s vic
    """
    print(inst)
    
def load_contacts():
    try:
        data = []
        with open('contacts.json') as f:
            data = json.load(f)
    except FileNotFoundError:
        print("Cannot find the database file contacts.json")
    except json.decoder.JSONDecodeError:
        print("Looks like contacts.json is corrupted. Check the file and try again")
    return data
    

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hn:ls:d:u:', ["help", "new=", "list", "search=", "delete=", "update="])
    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)
    

    for o,a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-n", "--new"):
            create_contact(a)
            list_contacts()
        elif o in ("-l", "--list"):
            list_contacts()
        elif o in ("-s", "--search"):
            found = search_contact("name", a)
            print("%s contacts found" % len(found))
            print(found)
        elif o in ("-d", "--delete"):
            delete_contact(a)
        elif o in ("-u", "--update"):
            update_contact(a)


if __name__ == "__main__":
    contacts = load_contacts()
    main()