# dalecontacto

## Goal

dalecontacto is an Albert plugin that helps you manage your Google contacts.

Usage:
ct -s Albert
ct -l
ct -n 943324234

## Current version:
- Command line tool to manage all your contacts stored in a json file